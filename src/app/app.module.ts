import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SocialLoginModule,AuthServiceConfig,GoogleLoginProvider,FacebookLoginProvider} from 'angular-6-social-login';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { ChatComponent } from './chat/chat.component';
import { RouterModule, Routes,Router } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { FormsModule} from '@angular/forms';
import {  CanActivate} from '@angular/router';
import { ShowDataService } from './show-data.service';


const route: Routes = [
  {
    path:'',
    component: SigninComponent
  },
  {
    path:'chat',
    component: ChatComponent,
    canActivate:[ShowDataService]
  },
  {
    path:'**',
    component:SigninComponent
  }
]

export function getAuthServiceConfigs(){
  let config= new AuthServiceConfig([
    {
      id:GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider('175471667632-ur8hk8pja2tmmrq2cooknte65p3cjhu5.apps.googleusercontent.com')
    }
    // {
    //   id: FacebookLoginProvider.PROVIDER_ID,
    //   provider: new FacebookLoginProvider("293358844759658")
    // }
  ]);
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    RouterModule.forRoot(route),
    HttpModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    {
      provide:AuthServiceConfig,
      useFactory:getAuthServiceConfigs
    }
  ],
    bootstrap: [AppComponent]
})
export class AppModule { }
