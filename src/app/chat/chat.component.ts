import { Component, OnInit } from '@angular/core';
import { ShowDataService } from '../show-data.service';
import { Router } from '../../../node_modules/@angular/router';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  constructor(private showServ: ShowDataService,private route:Router) {
  
   }
  ChannelName: string;
  message: string;
  allMessages = [];
   user= localStorage.getItem("identity");

  ngOnInit() {
    this.allMessages=[];
    this.myChannel=[];
    this.getMessages(localStorage.getItem("chid"));
    this.showChannel();
    //this.showMembers();
  }

  authen() {
    this.showServ.setData().subscribe(dat => { console.log("get call" + JSON.stringify(dat)) }), err => { console.log(err) }
  }
  //add new channel 
  addChannel(){
    
    this.showServ.addChannel(this.ChannelName).subscribe(dat => { console.log("get call" + JSON.stringify(dat)) }), err => { console.log(err) }

  }
  //send messages to api
  sendMessage() {
    
    this.showServ.sendMessage(this.message).subscribe(dat => { console.log("get call" + JSON.stringify(dat)) }), err => { console.log(err) }
    //this.ngOnInit();
    this.getMessages(localStorage.getItem("chid"));

  }

  /*getMessages(){
    this.showServ.getMessages().subscribe(dat =>{ this.allMessages = dat.messages;},
    err=>{console.log(err);
    })
  }*/
  // let messages: Array<any>;
  //get and append messages
channelName;

  getMessages(myChannelId) {
    
localStorage.setItem("chid",myChannelId);
    this.showServ.getMessages(myChannelId).subscribe(dat => {
    
      console.log(dat);

      var len = dat.messages.length;
      for (let i = 0; i < len; i++) {
       
      //  this.allMessages.push( l,dat.messages[i].body);
    // this.allMessages[i] = { msg: dat.messages[i].body, sender: true, senderId: l }
        // this.allMessages.push(dat.messages[i].body);//,dat.messages[i].from);
        if (dat.messages[i].from == this.idd)
        this.allMessages[i] = { msg: dat.messages[i].body, sender: dat.messages[i].from.split('@')[0], senderId: dat.messages[i].from }
      else
        this.allMessages[i] = { msg: dat.messages[i].body, sender:dat.messages[i].from.split('@')[0], senderId: dat.messages[i].from}
      }
    
      console.log(this.allMessages);
    })
    this.allMessages.length = 0;

}
  
  

  channelData: any = [];
  channel: string = "";
  found = "";
  foundId = "";
  Len;

  search() {

    this.showServ.search().subscribe(dat => {
      for (let index = 0; index < dat.channels.length; index++)
       {
        this.channelData.push(dat.channels[index].unique_name)
        this.Len = this.channelData.length;
        for (let index = 0; index < this.Len; index++)
        {
          if (this.channelData[index] == this.channel)
         { 
            console.log("channel found");
            localStorage.setItem("channel",this.channel);
            this.found = this.channel;
            localStorage.setItem("chId",this.foundId);
           this.foundId = dat.channels[index].sid;
             
            break;
          }
          else {
            console.log("not found");
           this.found = "channel not found";
          }
        }
      }
    },
      err => {
        console.log();
      })
  }


  logout(){
   // localStorage.clear();
    localStorage.removeItem("identity");
    this.route.navigate(['']);
  }

  join(){
    console.log(this.foundId);
    this.showServ.join(this.foundId).subscribe(res=>{
     // localStorage.setItem("chId",this.foundId);
      console.log(res);
    },err=>{
      console.log(err);
    })
  }
all:any=[];

channeldel:string;
delete(){
  this.showServ.delete(this.channeldel).subscribe(dat => { console.log("get call" + JSON.stringify(dat)) }), err => { console.log(err) }

}
channels:string;
list=[];
totalChannel=[];


showChannel(){
  this.myChannel.length = 0;
  this.showServ.showChannel().subscribe(dat => {
    this.totalChannel=dat.channels;
  // console.log(dat.channels.length);
   for(let i=0;i<dat.channels.length;i++)
   {
  //  console.log("get call" + (dat.channels[i].unique_name));
  this.list.push( {name:dat.channels[i].unique_name,id:dat.channels[i].sid});
}
this.showMembers(this.list);
  })
}


myChannel=[];
idd;
element1;
showMembers(list1){
  this.myChannel=[];
//  console.log(list.length,"list");
   list1.forEach(element => {
    this.element1=element;
 //   console.log(element.name+"list");
   this.showServ.showMembers(element.id).subscribe(dat=>{
  
     this.getList(dat);
 //  console.log(dat.members.length);
  
 
  
// //console.log(dat.members[0].identity);
   })
 });
 
}

getList(dat){
  
  for(let i=0;i<dat.members.length;i++){
    //  console.log( dat.members[i].identity);
      let idd=localStorage.getItem("identity");
      if(dat.members[i].identity==idd){
      this.totalChannel.forEach(element=>{
        if(dat.members[i].channel_sid==element.sid)
        {
          this.myChannel.push({name:element.unique_name ,id:element.sid});
        }
      })
       // console.log("in if",element.name,"ejfg",element.id);
     
//       this.idd=element.id;
   //    console.log(this.idd);
  
    }
//    // console.log(dat.members[i].identity);
//   //  { 
 
 }

// console.log("mychannel",this.myChannel);
 //console.log("mychannel",this.myChannel);
 //localStorage.setItem("channel name",this.myChannel);

}

username=localStorage.getItem("identity").split('@')[0];
}