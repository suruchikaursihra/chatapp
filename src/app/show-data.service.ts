import { Injectable } from '@angular/core';
import { Observable } from '../../node_modules/rxjs';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '../../node_modules/@angular/common/http';
import { HttpClient, } from '@angular/common/http';
import { map } from '../../node_modules/rxjs/operators';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ShowDataService implements CanActivate {


  url = "https://chat.twilio.com/v2/Services/";
  srId: string = "IS833d38baec3046e3bd626082c26923d0";
  chId: string = "CH395bb578074b4119bc6558819a68f570";
  //chId:string=localStorage.getItem("chId");
  identity: string = localStorage.getItem("identity");

  Options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      "Authorization": "Basic QUM1YjA5YmI5MDc0MGY2MjNlMDVlNGFkNjcwMTU2ZDY3YTo2NjIyMWU0MzRiZDJhYzA1MmU1MDljYzBhN2IyNmY0MQ=="
    })
  }

  constructor(private http: HttpClient, private route: Router) { }

  canActivate() {
    if (localStorage.getItem("identity") != null) {
      return true;
    }
    else {
      console.log("false");
      this.route.navigate(['/'])
      return false;
    }
  }

  setData(): Observable<any> {

    return this.http.post(this.url, "FriendlyName=chat", this.Options);
  }

  addChannel(ChannelName): Observable<any> {

    return this.http.post(this.url + this.srId + "/Channels", "FriendlyName=chat&UniqueName=" + ChannelName, this.Options);
  }

  sendMessage(message) {
    return this.http.post(this.url + this.srId + "/Channels/" + this.myChannelId + "/Messages", "ChannelSid=" + this.chId + "&ServiceSid=" + this.srId + "&Body=" + message + "&From=" + this.identity, this.Options);
  }
  myChannelId;
  
  getMessages(myChannelId): Observable<any> {
    this.myChannelId = myChannelId;
    return this.http.get(this.url + this.srId + "/Channels/" + myChannelId + "/Messages", this.Options).pipe(map(data => data));
  }

  search(): Observable<any> {
    return this.http.get(this.url + this.srId + "/Channels", this.Options).pipe(map(data => data));
  }

  join(chId): Observable<any> {
    return this.http.post(this.url + this.srId + "/Channels/" + chId + "/Members", "ChannelSid=" + this.chId + "&Identity=" + this.identity + "&ServiceSid=" + this.srId, this.Options);
  }

  chIdentity: string = localStorage.getItem("chId");

  delete(Channeldel): Observable<any> {
    return this.http.delete(this.url + this.srId + "/Channels" + this.chId, this.Options);
  }

  showChannel(): Observable<any> {
    return this.http.get(this.url + this.srId + "/Channels", this.Options).pipe(map(data => data));
  }

  showMembers(channel): Observable<any> {
    return this.http.get(this.url + this.srId + "/Channels/" + channel + "/Members", this.Options).pipe(map(data => data));

  }

}
