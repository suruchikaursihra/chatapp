import { Component, OnInit } from '@angular/core';
import { GoogleLoginProvider,FacebookLoginProvider } from '../../../node_modules/angular-6-social-login';
import { Router,Routes} from '@angular/router';
import { AuthService} from 'angular-6-social-login';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private socialAuthService: AuthService,private route:Router) { 
    //localStorage.setItem("chId",'CHe354f64b88084b7b8c2c903d71c6a920');
  }

  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    // if(socialPlatform == "facebook"){
    //   socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    // }
    
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData); //user details: gmail
        localStorage.setItem("identity",userData.email);
       // localStorage.setItem("chId",'CHe354f64b88084b7b8c2c903d71c6a920');
       this.route.navigate(['chat']);
      }
      
    );
  }



  ngOnInit() {
  }  

}
